#!/bin/bash
# Prunes all deployments from default namespace and deploys the given scenario file.
set -eu

kubectl config use-context kind-kind 

# Build operator
pushd ../operator
./build-and-run.sh
popd

# Build images
pushd ../smartgrid
./build-and-run.sh
popd

# Redeploy CRD in kind cluster
kubectl delete -f *.yaml 2>/dev/null ||:
sleep 1
kubectl apply -f $1.yaml

while true; do
	echo "Awaiting readiness..."
	sleep 5
	kubectl get pod --no-headers | grep -v "Running" && continue
	kubectl wait --timeout=10s --for=condition=available deployment smartgridems-$1 || continue
	kubectl wait --timeout=10s --for=condition=available deployment smartgridcommunity-$1 || continue
	curl -v http://localhost/$1/community 2>&1 | grep '"online_member_rate":1.0' &>/dev/null || continue

	echo "All Online."
	sleep 5
	break
done
